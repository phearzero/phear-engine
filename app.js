﻿//Instance Global
var INSTANCE = {
    processVerisons: process.versions,
    processTitle: process.title,
    processPID: process.pid,
    startTime: process.hrtime(),
    platform: process.platform,
    arch: process.arch,
    memory: process.memoryUsage(),
    uptime: process.uptime()
};
//OS information
var os = require('os');
//Log ENV
console.log(INSTANCE);
//Loggly Pipe
var loggly = require('loggly');
var client = loggly.createClient({
    token: "7efdc96a-e6f6-4f46-b709-b24b54ffc993",
    subdomain: "phearzero",
    tags: ["NodeJS"],
    json: true
});
client.log('Logger Online');

//Docker Controller
var docker = require('docker.io')({
    socketPath: '/var/run/docker.sock'
});

//Temporary benchmark function for execution time
var logTime = function(time) {
    var diff = process.hrtime(time);
    console.log('benchmark took %d nanoseconds', diff[0] * 1e9 + diff[1]);
}



//Utils
//var util = require('util');
//console.log(util.inspect(process.memoryUsage()));

//Next In Loop
//process.nextTick(function() {
//    console.log('nextTick callback');
//});
